<?php

namespace Infotechnohelp\Symfony\BundleInstallerBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveBundles extends BundlesBase
{
    protected static $defaultName = 'bundles:remove';

    /** @todo Implement later
    protected function configure()
    {
        parent::configure();

        $this
            ->addArgument('bundleTitle', InputArgument::OPTIONAL, 'Bundle title from /bundles.yml');
    }
    */

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        foreach ($this->bundleList as $bundlePackageTitle) {

            /** @todo Implement later
            $bundleTitleArgument = $input->getArgument('bundleTitle');

            if($bundleTitleArgument !== null && $bundleTitleArgument !== $bundlePackageTitle){
                continue;
            }
            */
            $output->writeln(
                "<fg=green>composer remove {$this->remoteRepoUsername}/$bundlePackageTitle</>"
            );
            $options = $input->getOption('options') ?? null;
            exec("composer remove {$this->remoteRepoUsername}/$bundlePackageTitle $options");

            $composerData = json_decode(file_get_contents($this->projectRoot . "composer.json"), true);

            $repositoryConfigIndex =
                $this->getComposerRepositoryConfigIndex(
                    $composerData['repositories'] ?? [],
                    $this->remoteRepoUsername. "/$bundlePackageTitle"
                );

            $bundleUrl = null;

            // Repository config exists
            if( $repositoryConfigIndex !== null){
                $output->writeln(
                    "<fg=green>{$this->remoteRepoUsername}/$bundlePackageTitle: remove composer repositories config</>"
                );
                $bundleUrl = $composerData['repositories'][$repositoryConfigIndex]['url'];
                unset($composerData['repositories'][$repositoryConfigIndex]);

                $composerData['repositories'] = array_values($composerData['repositories']);

                file_put_contents($this->projectRoot .'composer.json', json_encode($composerData, JSON_PRETTY_PRINT));
            }

            $output->writeln(
                "<fg=green>remove if exists " . $this->projectRoot . "$bundleUrl/</>"
            );
            exec("rm -rf " . $this->projectRoot . "$bundleUrl/");
        }

        if(count(glob($this->projectRoot . $this->bundlesDir . "/*")) === 0){
            $output->writeln(
                "<fg=green>remove empty " . $this->projectRoot . $this->bundlesDir . "</>"
            );
            exec("rm -rf " . $this->projectRoot . $this->bundlesDir . "/");
        }

        return Command::SUCCESS;
    }
}