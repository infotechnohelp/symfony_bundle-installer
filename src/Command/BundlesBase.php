<?php

namespace Infotechnohelp\Symfony\BundleInstallerBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Yaml\Yaml;

class BundlesBase extends Command
{
    protected static $defaultName = 'bundles:base-command';

    protected string $projectRoot;

    protected array $bundleList;

    protected string $bundlesDir;
    protected string $remoteRepoHost;
    protected string $remoteRepoUsername;

    public function __construct(string $name = null, ParameterBagInterface $params)
    {
        parent::__construct($name);

        $this->projectRoot = $params->get('kernel.project_dir') . DIRECTORY_SEPARATOR;
        $this->bundleList = Yaml::parse(file_get_contents($this->projectRoot . "bundles.yml"));
        $this->remoteRepoHost = $params->get('bundles.remoteRepoHost');
        $this->remoteRepoUsername = $params->get('bundles.remoteRepoUsername');
        $this->bundlesDir = $params->get('bundles.dir');
    }

    protected function configure()
    {
        $this
            ->addOption('options', null, InputOption::VALUE_OPTIONAL, 'Composer command options');
    }


    protected function getComposerRepositoryConfigIndex(array $repositoryConfigs, string $bundle)
    {
        foreach ($repositoryConfigs as $index => $config) {
            if (isset($config['bundle']) && $config['bundle'] === $bundle) {
                return $index;
            }
        }

        return null;
    }
}