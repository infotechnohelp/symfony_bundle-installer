<?php

namespace Infotechnohelp\Symfony\BundleInstallerBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class InstallBundles extends BundlesBase
{
    protected static $defaultName = 'bundles:install';

    private function askForPassword(InputInterface $input, OutputInterface $output): string
    {
        $helper = $this->getHelper('question');

        $question = new Question('What is the GIT repo password?');
        $question->setHidden(true);
        $question->setHiddenFallback(false);

        return $helper->ask($input, $output, $question);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $password = $this->askForPassword($input, $output);

        foreach ($this->bundleList as $bundlePackageTitle) {

            $remoteRepoUrl = "https://{$this->remoteRepoUsername}:$password@{$this->remoteRepoHost}/" .
                "{$this->remoteRepoUsername}/$bundlePackageTitle.git";

            $output->writeln(
                "<fg=green>git clone {$this->remoteRepoUsername}/$bundlePackageTitle {$this->projectRoot}tmp/bundle</>"
            );

            exec("git clone $remoteRepoUrl {$this->projectRoot}tmp/bundle");

            try {
                $bundleComposerData = json_decode(
                    file_get_contents($this->projectRoot . "tmp/bundle/composer.json"),
                    true
                );

            } catch (\Exception $e) {

                if (str_ends_with($e->getMessage(), "Failed to open stream: No such file or directory")) {
                    $output->writeln("<fg=red>A wrong password provided</>");
                    return Command::FAILURE;
                }
            }

            $bundleNamespace = array_keys($bundleComposerData['autoload']['psr-4'])[0];

            $explodedBundleNamespace = explode('\\', $bundleNamespace);

            $bundleTitle = $explodedBundleNamespace[count($explodedBundleNamespace) - 2];

            $bundlePath = $this->projectRoot . $this->bundlesDir . "/$bundleTitle";

            if (file_exists($bundlePath)) {
                exec("rm -rf {$this->projectRoot}tmp/bundle");
            } else {
                $output->writeln(
                    "<fg=green>{$this->remoteRepoUsername}/$bundlePackageTitle: move to " .
                    $this->projectRoot . $this->bundlesDir . "</>"
                );
                // Ignored if exists
                @mkdir($this->projectRoot . $this->bundlesDir, 0755, true);

                exec("mv {$this->projectRoot}tmp/bundle $bundlePath");
            }


            $composerData = json_decode(file_get_contents($this->projectRoot . "composer.json"), true);

            // Repository config does not exist
            if ($this->getComposerRepositoryConfigIndex($composerData['repositories'] ?? [], "{$this->remoteRepoUsername}/$bundlePackageTitle") === null) {

                $output->writeln(
                    "<fg=green>{$this->remoteRepoUsername}/$bundlePackageTitle: update composer repositories</>"
                );

                $composerData['repositories'][] = [
                    'type' => 'path',
                    'url' => $this->bundlesDir . "/$bundleTitle",
                    'bundle' => "{$this->remoteRepoUsername}/$bundlePackageTitle",
                ];

                $composerData['repositories'] = array_values($composerData['repositories']);

                file_put_contents($this->projectRoot . 'composer.json', json_encode($composerData, JSON_PRETTY_PRINT));
            }

            $options = $input->getOption('options') ?? null;

            if (!in_array($this->remoteRepoUsername . "/$bundlePackageTitle", array_keys($composerData['require']))) {
                $output->writeln("<fg=green>composer require {$this->remoteRepoUsername}/$bundlePackageTitle</>");
                exec("composer require {$this->remoteRepoUsername}/$bundlePackageTitle:dev-master $options");
            }
        }

        /*
        Avoid strange cache error:
        In App_KernelDevDebugContainer.php line 445:
        Warning: require(/var/www/html/var/cache/dev/ContainerUL0w0hx/getConsole_ErrorListenerService.php): Failed to open stream: No such file or directory
        */
        $output->writeln("<fg=green>Success</>");
        exit; //return Command::SUCCESS;
    }
}